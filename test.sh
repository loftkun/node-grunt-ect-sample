#! /bin/bash

PACKAGE_NAME=sample
IMAGE_NAME=node-grunt-ect
CONTAINER_NAME=node-grunt-ect-test
DOCKERFILE_NAME=Dockerfile
#DOCKERFILE_NAME=Dockerfile-use-ubuntu

# build image
rm -fr ./${PACKAGE_NAME}/node_modules
rm -fr ./${PACKAGE_NAME}/dest/*
sudo docker build -t ${IMAGE_NAME} . -f ${DOCKERFILE_NAME}
result=$?
if [ ${result} -ne 0 ]; then
	exit 99
fi

# run container
sudo docker stop ${CONTAINER_NAME}
sudo docker rm ${CONTAINER_NAME}
sudo docker run -d --name ${CONTAINER_NAME} ${IMAGE_NAME}

# exec
# do task
# sudo docker exec -it ${CONTAINER_NAME} /${PACKAGE_NAME}/node_modules/grunt/bin/grunt
sudo docker exec -it ${CONTAINER_NAME} npm run grunt

# get artifacts
sudo rm -fr ./artifacts
sudo docker cp ${CONTAINER_NAME}:/${PACKAGE_NAME}/dest ./artifacts
cat ./artifacts/index.html


