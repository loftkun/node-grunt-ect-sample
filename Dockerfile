FROM node:11.13

ENV PACKAGE_NAME=sample

WORKDIR /
COPY ./${PACKAGE_NAME} ./${PACKAGE_NAME}

WORKDIR /${PACKAGE_NAME}
RUN npm install

CMD tail -f /dev/null
