module.exports = (grunt) ->
  pkg = grunt.file.readJSON 'package.json'

  grunt.initConfig
    dir:
      src: "src"
      dest: "dest"
    ect:
      options:
        root: '<%= dir.src %>' # テンプレート側のパスを設定
      render:
        expand: true # あとに続くオプションを有効に
        cwd: '<%= dir.src %>'
        src: ['**/*.html.ect']
        dest: '<%= dir.dest %>'
        ext: '.html'
        variables:
            projectName : 'サイトタイトル'
            links: [
              { name: 'Google',   url: 'http://google.com/'   },
              { name: 'Facebook', url: 'http://facebook.com/' },
              { name: 'Twitter',  url: 'http://twitter.com/'  },
            ]

    # プラグインload
    grunt.loadNpmTasks 'grunt-ect'

    #タスク登録
    grunt.registerTask "default", ["ect"]
